#! /bin/bash
#
# convert-keys.sh
# Copyright (C) 2020 Maxime “pep” Buquet <pep@bouah.net>
#
# Distributed under terms of the GPLv3+ license.
#

set -euo pipefail

# GitLab seems to remove newlines from CI variables, preventing gnupg from
# reading our keys.
function normalize_key() {
  local orig_key="$*"
  local key_type stripped_key

  key_type=$(echo "${orig_key}" | sed 's,.*-----BEGIN PGP \(PUBLIC\|PRIVATE\).*,\1,')
  stripped_key=$(
    echo "${orig_key}" | \
    sed 's,\(\s*\)\?-----[^-]\+-----\(\s*\)\?,,g;s,\(\S\+\)\s\+,\1\n,g'
  )

  echo -ne "-----BEGIN PGP ${key_type} KEY BLOCK-----\n\n${stripped_key}\n-----END PGP ${key_type} KEY BLOCK-----\n"
}

normalize_key "$@"
