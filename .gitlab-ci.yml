image: 'archlinux/archlinux:base-devel'

stages:
  - build

.build-repo:
  stage: build
  variables:
    REPODIR: "$CI_PROJECT_DIR"
    BUILDDIR: "/home/nobody/build"
    REPONAME: "cluxia-$CI_COMMIT_REF_NAME"
    SIGNING_PUB_FP: "0CD095515B6FB54FB0EDCCD2C8673DA556151388"
  script:
      - pacman-key --init
      # Update build environment
      - pacman -Syu --noconfirm

      # The repository build script is run by user 'nobody'. The build scripts needs
      # to install dependencies using pacman. This requires root permissions.
      # (Note: the build script itself can't be run as root, as makepkg would
      # complain)
      - "echo 'nobody ALL=(ALL) NOPASSWD: /usr/bin/pacman' >> /etc/sudoers"

      # a writable home-directory is needed by import-validpgpkeys.sh
      # additionally the build directory must be owned by the user doing makepkg
      - mkdir -p /home/nobody/build && chown -R nobody /home/nobody
      - usermod -d /home/nobody nobody

      # import pgp keys flagged as valid
      - sudo -u nobody -E -H .build-bin/import-validpgpkeys.sh

      - |
        # add repository for locally built packages
        echo "[$REPONAME]
        Server = file://$REPODIR
        SigLevel = Never" >> /etc/pacman.conf

      # Custom settings
      - cp makepkg.conf /etc/makepkg.conf

      # Package signing
      - chown nobody -R /home/nobody/.gnupg
      - chmod 700 /home/nobody/.gnupg && chmod 600 /home/nobody/.gnupg/*
      - sudo -u nobody -E -H .build-bin/convert-keys.sh $SIGNING_PUB_KEY > /tmp/signing_pub_key.gpg
      - sudo -u nobody -E -H .build-bin/convert-keys.sh $SIGNING_PRIV_KEY > /tmp/signing_priv_key.gpg
      - sudo -u nobody -E -H gpg --import /tmp/signing_pub_key.gpg /tmp/signing_priv_key.gpg

      - sudo -u nobody -E -H .build-bin/update-repo.sh

      # the build script can't be run as root, as makepkg would complain...
      - sudo -u nobody -E -H .build-bin/build.sh

      # publish repository index
      # -> pacman expects files without tar.xz prefix,
      # -> webfronted of gitlab ci does not handle symlinks
      # => replace existing symlinks for artifacts upload (only)
      #    keep symlinks when building locally without artifact upload
      - |
        if [[ -n $CI_PROJECT_URL ]]; then # Prepare repo index for publishing
          mv --force $REPONAME.db.tar.xz $REPONAME.db
          mv --force $REPONAME.db.tar.xz.sig $REPONAME.db.sig
          mv --force $REPONAME.files.tar.xz $REPONAME.files
          mv --force $REPONAME.files.tar.xz.sig $REPONAME.files.sig
        fi

.notify-webhook:
  script:
    - |
      curl --fail -H "Content-Type: application/json" -H "Token: $REPO_TOKEN" $REPO_WEBHOOK_URL -d "{\"ref\": \"$CI_COMMIT_REF_NAME\"}"

.generate-artifacts:
  artifacts:
    # expire artifacts per default - the gitlab web frontend can be used to keep
    # artifacts of interest for an unlimited time
    expire_in: 2 weeks
    paths:
      - "$REPONAME.db"
      - "$REPONAME.db.sig"
      - "$REPONAME.files"
      - "$REPONAME.files.sig"
      - "*.pkg.tar.xz"
      - "*.pkg.tar.xz.sig"

build-x86_64:
  extends:
    - .build-repo
  except:
    refs:
      - master

build-x86_64-cluxia:
  extends:
    - .build-repo
    - .generate-artifacts
  variables:
    REPONAME: "cluxia"
  only:
    refs:
      - master

notify-webhook:
  stage: .post
  extends:
    - .notify-webhook
  only:
    refs:
      - master
