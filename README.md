# Cluxia packages

This package repository contains various packages used in our infrastructure.
See [cluxia/infrastructure](https://gitlab.com/cluxia/infrastructure).

# License

This project is licensed under
[GPL-3.0-or-later](https://spdx.org/licenses/GPL-3.0-or-later.html) unless
specified.

This project is based on [archlinux-overlay](https://github.com/episource/archlinux-overlay)
(Apache-2.0), which is based on
[MINGW-packages](https://github.com/Alexpux/MINGW-packages/blob/master/ci-library.sh) (3-clause
BSD).
